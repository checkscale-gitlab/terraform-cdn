provider "aws" {
  region = "${var.region}"
}

terraform {
  required_version = ">= 0.11.13"

  backend "s3" {}
}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    region         = "${var.region}"
    bucket         = "sk-prd-terraform-state"
    key            = "terraform-vpc/${var.environment}/terraform.state"
    encrypt        = "true"
    dynamodb_table = "sk-prd-terraform-state-lock"
  }
}

locals {
  tags   = "${merge(var.tags, map("Environment", "${var.environment}"))}"
  domain = "${var.stage}.${var.main_domain}"
}

module "cdn" {
  source  = "git::https://github.com/cloudposse/terraform-aws-cloudfront-s3-cdn.git"
  version = "0.8.0"
  enabled = false

  namespace           = "${var.namespace}"
  stage               = "${var.stage}"
  name                = "${var.name}"
  aliases             = ["assets.${local.domain}"]
  parent_zone_name    = "${var.main_domain}"
  acm_certificate_arn = "${var.acm_certificate_arn}"
}

resource "aws_s3_bucket_object" "index" {
  bucket       = "${module.cdn.s3_bucket}"
  key          = "index.html"
  source       = "${path.module}/index.html"
  content_type = "text/html"
  etag         = "${md5(file("${path.module}/index.html"))}"
}
